package com.tora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamsExercisesUtils {

    public static BigDecimal computeSumOfBigDecimalsList(List<BigDecimal> bigDecimals) {
        return bigDecimals
                .stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    public static BigDecimal computeAverageOfBigDecimalsList(List<BigDecimal> bigDecimals) {
        Optional<BigDecimal[]> averageResultOptional = bigDecimals
                .stream()
                .filter(Objects::nonNull)
                .map(bigDecimal -> new BigDecimal[]{bigDecimal, BigDecimal.ONE})
                .reduce((firstBigDecimal, secondBigDecimal) -> {
                    firstBigDecimal[0] = firstBigDecimal[0].add(secondBigDecimal[0]);
                    firstBigDecimal[1] = firstBigDecimal[1].add(secondBigDecimal[1]);
                    return firstBigDecimal;
                });

        if (averageResultOptional.isPresent()) {
            BigDecimal[] averageResult = averageResultOptional.get();
            return averageResult[0].divide(averageResult[1], RoundingMode.HALF_EVEN);
        }

        throw new IllegalArgumentException();
    }

    public static List<BigDecimal> getTopTenPercentFromBigDecimalsList(List<BigDecimal> bigDecimals) {
        int tenPercentSize = (int) Math.ceil(bigDecimals.stream().filter(Objects::nonNull).count() / 10.0);

        return bigDecimals
                .stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.reverseOrder())
                .limit(tenPercentSize)
                .collect(Collectors.toList());
    }

}