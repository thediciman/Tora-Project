package com.tora;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class StreamsExercisesTest {

    @Test
    public void computeSumOfBigDecimalsListWhenGivenEmptyListShouldReturnZero() {
        // Arrange
        List<BigDecimal> bigDecimals = Collections.emptyList();

        // Act
        BigDecimal expected = BigDecimal.ZERO;
        BigDecimal actual = StreamsExercisesUtils.computeSumOfBigDecimalsList(bigDecimals);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void computeSumOfBigDecimalsListWhenGivenNonEmptyListShouldReturnCorrectSum() {
        // Arrange
        List<BigDecimal> bigDecimals = Arrays.asList(
                new BigDecimal(5),
                new BigDecimal(3),
                new BigDecimal(-5),
                new BigDecimal(42)
        );

        // Act
        BigDecimal expected = BigDecimal.ZERO;
        for (BigDecimal bigDecimal : bigDecimals) {
            expected = expected.add(bigDecimal);
        }

        BigDecimal actual = StreamsExercisesUtils.computeSumOfBigDecimalsList(bigDecimals);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void computeAverageOfBigDecimalsListWhenGivenEmptyListShouldThrowException() {
        // Arrange
        List<BigDecimal> bigDecimals = Collections.emptyList();

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> StreamsExercisesUtils.computeAverageOfBigDecimalsList(bigDecimals));
    }

    @Test
    public void computeAverageOfBigDecimalsListWhenGivenNonEmptyListReturnCorrectAverage() {
        // Arrange
        List<BigDecimal> bigDecimals = Arrays.asList(
                new BigDecimal(5),
                new BigDecimal(3),
                new BigDecimal(-5),
                new BigDecimal(42)
        );

        // Act
        BigDecimal expected = BigDecimal.ZERO;
        for (BigDecimal bigDecimal : bigDecimals) {
            expected = expected.add(bigDecimal);
        }
        expected = expected.divide(new BigDecimal(bigDecimals.size()), RoundingMode.HALF_EVEN);

        BigDecimal actual = StreamsExercisesUtils.computeAverageOfBigDecimalsList(bigDecimals);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getTopTenPercentFromBigDecimalsListWhenSizeIsLessThanTenShouldOnlyPrintBiggestElement() {
        // Arrange
        List<BigDecimal> expected = Collections.singletonList(new BigDecimal(42));

        // Act
        List<BigDecimal> actual = StreamsExercisesUtils.getTopTenPercentFromBigDecimalsList(expected);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getTopTenPercentFromBigDecimalsListWhenSizeIsEqualToTenShouldOnlyPrintBiggestElement() {
        // Arrange
        List<BigDecimal> bigDecimals = IntStream
                .rangeClosed(1, 10)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        // Act
        List<BigDecimal> expected = Collections
                .singletonList(
                        bigDecimals
                                .stream()
                                .max(BigDecimal::compareTo)
                                .orElse(BigDecimal.ZERO)
                );

        List<BigDecimal> actual = StreamsExercisesUtils.getTopTenPercentFromBigDecimalsList(bigDecimals);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getTopTenPercentFromBigDecimalsListWhenSizeIsGreaterThanTenShouldPrintTopTenPercentValues() {
        // Arrange
        List<BigDecimal> bigDecimals = IntStream
                .rangeClosed(1, 100)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        // Act
        List<BigDecimal> expected = IntStream
                .rangeClosed(91, 100)
                .mapToObj(BigDecimal::new)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        List<BigDecimal> actual = StreamsExercisesUtils.getTopTenPercentFromBigDecimalsList(bigDecimals);

        // Assert
        assertEquals(expected, actual);
    }

}