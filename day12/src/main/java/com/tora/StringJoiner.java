package com.tora;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class StringJoiner {

    private final List<String> tokenList;
    private String delimiter;

    StringJoiner() {
        tokenList = new ArrayList<>();
        delimiter = null;
    }

    void addToken(String token) {
        tokenList.add(token);
    }

    void removeToken(String token) {
        tokenList.removeAll(Collections.singletonList(token));
    }

    void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    String getString() {
        if (delimiter == null) {
            throw new IllegalStateException();
        }

        StringBuilder joinedString = new StringBuilder();
        tokenList.forEach(token -> joinedString.append(token).append(delimiter));
        return joinedString.substring(0, joinedString.length() - delimiter.length());
    }

}