package com.tora;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class StringTokenizer {

    private List<String> tokens;
    private String delimiter;
    private String stringToBeTokenized;

    int countTokens() {
        if (tokens == null) {
            throw new IllegalStateException();
        }

        return tokens.size();
    }

    List<String> getTokens() {
        if (tokens == null) {
            throw new IllegalStateException();
        }

        return tokens;
    }

    String getTokenAt(int index) {
        if (tokens == null) {
            throw new IllegalStateException();
        }

        return tokens.get(index);
    }

    void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
        tryToTokenize();
    }

    void setString(String stringToBeTokenized) {
        this.stringToBeTokenized = stringToBeTokenized;
        tryToTokenize();
    }

    private void tryToTokenize() {
        if (delimiter != null && stringToBeTokenized != null) {
            tokens = Arrays
                    .stream(stringToBeTokenized.split(Pattern.quote(delimiter)))
                    .filter(string -> string.length() > 0)
                    .collect(Collectors.toList());
        }
    }

}