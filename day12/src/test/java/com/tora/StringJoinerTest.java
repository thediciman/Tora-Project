package com.tora;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class StringJoinerTest {

    private StringJoiner stringJoiner;

    @Before
    public void setUp() {
        stringJoiner = new StringJoiner();
    }

    @Test
    public void getStringWhenDelimiterIsNotSetShouldThrowException() {
        // Act and Assert
        assertThrows(IllegalStateException.class, () -> stringJoiner.getString());
    }

    @Test
    public void addTokenShouldAddTokensToFinalString() {
        // Arrange
        stringJoiner.setDelimiter("");
        stringJoiner.addToken("T");
        stringJoiner.addToken("o");
        stringJoiner.addToken("r");
        stringJoiner.addToken("a");

        // Act
        String expected = "Tora";
        String actual = stringJoiner.getString();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void removeTokenShouldRemoveAllGivenTokenAppearancesFromFinalString() {
        // Arrange
        stringJoiner.setDelimiter(".");
        stringJoiner.addToken("127");
        stringJoiner.addToken("0");
        stringJoiner.addToken("0");
        stringJoiner.addToken("1");
        stringJoiner.removeToken("0");

        // Act
        String expected = "127.1";
        String actual = stringJoiner.getString();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getStringWhenDelimiterIsSetAndTokensWereAddedShouldReturnCorrectResult() {
        // Arrange
        stringJoiner.setDelimiter(".");
        stringJoiner.addToken("127");
        stringJoiner.addToken("0");
        stringJoiner.addToken("0");
        stringJoiner.addToken("1");

        // Act
        String expected = "127.0.0.1";
        String actual = stringJoiner.getString();

        // Assert
        assertEquals(expected, actual);
    }


}