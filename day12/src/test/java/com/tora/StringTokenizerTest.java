package com.tora;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class StringTokenizerTest {

    private StringTokenizer stringTokenizer;

    @Before
    public void setUp() {
        stringTokenizer = new StringTokenizer();
    }

    @Test
    public void countTokensWhenDelimiterIsNotSetShouldThrowException() {
        // Arrange
        stringTokenizer.setString("some string");

        // Act and Assert
        assertThrows(IllegalStateException.class, () -> stringTokenizer.countTokens());
    }

    @Test
    public void countTokensWhenStringIsNotSetShouldThrowException() {
        // Arrange
        stringTokenizer.setDelimiter("delimiter");

        // Act and Assert
        assertThrows(IllegalStateException.class, () -> stringTokenizer.countTokens());
    }

    @Test
    public void getTokensWhenDelimiterIsNotSetShouldThrowException() {
        // Arrange
        stringTokenizer.setString("some string");

        // Act and Assert
        assertThrows(IllegalStateException.class, () -> stringTokenizer.getTokens());
    }

    @Test
    public void getTokensWhenStringIsNotSetShouldThrowException() {
        // Arrange
        stringTokenizer.setDelimiter("delimiter");

        // Act and Assert
        assertThrows(IllegalStateException.class, () -> stringTokenizer.getTokens());
    }

    @Test
    public void getTokenAtWhenDelimiterIsNotSetShouldThrowException() {
        // Arrange
        stringTokenizer.setString("some string");

        // Act and Assert
        assertThrows(IllegalStateException.class, () -> stringTokenizer.getTokenAt(0));
    }

    @Test
    public void getTokenAtWhenStringIsNotSetShouldThrowException() {
        // Arrange
        stringTokenizer.setDelimiter("delimiter");

        // Act and Assert
        assertThrows(IllegalStateException.class, () -> stringTokenizer.getTokenAt(0));
    }

    @Test
    public void countTokensWhenStringAndDelimitersAreSetShouldReturnCorrectResult() {
        // Arrange
        stringTokenizer.setDelimiter(".");
        stringTokenizer.setString("127.0.0.1");

        // Act
        int expected = 4;
        int actual = stringTokenizer.countTokens();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getTokensWhenStringAndDelimitersAreSetShouldReturnCorrectResult() {
        // Arrange
        stringTokenizer.setDelimiter(".");
        stringTokenizer.setString("127.0.0.1");

        // Act
        List<String> expected = Arrays.asList("127", "0", "0", "1");
        List<String> actual = stringTokenizer.getTokens();

        // Assert
        assertEquals(expected, actual);
    }


    @Test
    public void getTokenAtWhenStringAndDelimitersAreSetShouldReturnCorrectResult() {
        // Arrange
        stringTokenizer.setDelimiter(".");
        stringTokenizer.setString("127.0.0.1");

        // Act
        String expected = "127";
        String actual = stringTokenizer.getTokenAt(0);

        // Assert
        assertEquals(expected, actual);
    }

}