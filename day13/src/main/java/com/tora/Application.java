package com.tora;

import com.tora.entities.Person;
import com.tora.repository.IRepository;
import com.tora.repository.impl.InMemoryRepository;
import com.tora.service.PersonService;
import com.tora.validators.CNPValidator;
import com.tora.validators.EmailValidator;
import com.tora.validators.NameValidator;
import com.tora.validators.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Application {

    private static void readAndSerialize() throws Exception {
        final int fileIndex = 0;

        Validator<Person> personValidator = new CNPValidator(new EmailValidator(new NameValidator(null)));

        IRepository<Person> nonSerializedPersonRepository = new InMemoryRepository<>();
        PersonService nonSerializedPersonService = new PersonService(nonSerializedPersonRepository, personValidator);

        nonSerializedPersonService.populateRepositoryWithObjectsFromFile("day13\\day13_input" + fileIndex + ".txt");

        nonSerializedPersonService.saveRepositorySerializedObjectsToFile("day13\\day13_input" + fileIndex + "_serialized.dat");

        IRepository<Person> serializedPersonRepository = new InMemoryRepository<>();
        PersonService serializedPersonService = new PersonService(serializedPersonRepository, personValidator);

        serializedPersonService.populateRepositoryWithSerializedObjectsFromFile("day13\\day13_input" + fileIndex + "_serialized.dat");
    }

    public static void main(String[] args) throws Exception {

        for (int i = 0; i < 5; ++i) {
            System.out.println("RUN #" + i);

            Validator<Person> personValidator = new CNPValidator(new EmailValidator(new NameValidator(null)));
            IRepository<Person> personRepository = new InMemoryRepository<>();
            PersonService personService = new PersonService(personRepository, personValidator);

            List<String> filePaths = new ArrayList<>();
            IntStream.rangeClosed(0, 3).forEach(index -> filePaths.add("day13\\day13_input" + 3 + ".txt"));

            long startTime = System.currentTimeMillis();
            personService.multithreadedPopulateRepositoryWithObjectsFromFiles(filePaths);
            long endTime = System.currentTimeMillis();

            long deltaTime = endTime - startTime;
            System.out.println("Time elapsed: " + deltaTime + "ms.");
            System.out.println("Number of entities in the repository: " + personRepository.getAll().size() + "\n");
        }

    }

}