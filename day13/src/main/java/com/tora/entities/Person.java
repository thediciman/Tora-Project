package com.tora.entities;

import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 6942069420694206969L;

    private String firstName;
    private String middleName;
    private String lastName;
    private String CNP;
    private String email;

    public Person(String firstName, String middleName, String lastName, String CNP, String email) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.CNP = CNP;
        this.email = email;
    }

    private Person(Builder builder) {
        this(builder.firstName, builder.middleName, builder.lastName, builder.CNP, builder.email);
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", CNP='" + CNP + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCNP() {
        return CNP;
    }

    public String getEmail() {
        return email;
    }

    public enum TokenIndices {
        FIRST_NAME(0),
        MIDDLE_NAME(1),
        LAST_NAME(2),
        CNP(3),
        EMAIL(4);

        private final int index;

        TokenIndices(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }

    public static class Builder {
        private String firstName = "";
        private String middleName = "";
        private String lastName = "";
        private String CNP = "";
        private String email = "";

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withMiddleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withCNP(String CNP) {
            this.CNP = CNP;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }
}