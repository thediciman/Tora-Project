package com.tora.repository;

import java.util.List;

public interface IRepository<T> {
    void add(T element);

    List<T> getAll();
}