package com.tora.repository.impl;

import com.tora.repository.IRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryRepository<T> implements IRepository<T> {

    private final List<T> data;

    public InMemoryRepository() {
        data = new ArrayList<>();
    }

    @Override
    synchronized public void add(T element) {
        data.add(element);
    }

    @Override
    public List<T> getAll() {
        return data;
    }

}