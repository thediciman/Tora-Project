package com.tora.service;

import com.tora.entities.Person;
import com.tora.repository.IRepository;
import com.tora.validators.Validator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PersonService {

    private final IRepository<Person> repository;
    private final Validator<Person> validator;

    public PersonService(IRepository<Person> personRepository, Validator<Person> validator) {
        this.repository = personRepository;
        this.validator = validator;
    }

    public void populateRepositoryWithObjectsFromFile(String path) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        String[] lines = bufferedReader.readLine().split("#");

        for (String line : lines) {
            String[] tokens = line.split("\\|");

            Person currentPerson = new Person.Builder()
                    .withFirstName(tokens[Person.TokenIndices.FIRST_NAME.getIndex()])
                    .withMiddleName(tokens[Person.TokenIndices.MIDDLE_NAME.getIndex()])
                    .withLastName(tokens[Person.TokenIndices.LAST_NAME.getIndex()])
                    .withCNP(tokens[Person.TokenIndices.CNP.getIndex()])
                    .withEmail(tokens[Person.TokenIndices.EMAIL.getIndex()])
                    .build();

            if (validator.validate(currentPerson)) {
                repository.add(currentPerson);
            }
        }
    }

    public void singlethreadedPopulateRepositoryWithObjectsFromFiles(List<String> filePaths) throws InterruptedException {
        Thread thread = new Thread(() -> {
            filePaths.forEach(filePath -> {
                try {
                    this.populateRepositoryWithObjectsFromFile(filePath);
                } catch (IOException ioe) {
                    throw new IllegalArgumentException(ioe.getMessage());
                }
            });
        });
        thread.start();
        thread.join();
    }

    public void multithreadedPopulateRepositoryWithObjectsFromFiles(List<String> filePaths) {
        List<Thread> threads = new ArrayList<>(filePaths.size());

        filePaths.forEach(filePath -> {
            Thread currentThread = new Thread(() -> {
                try {
                    this.populateRepositoryWithObjectsFromFile(filePath);
                } catch (IOException ioe) {
                    throw new IllegalArgumentException(ioe.getMessage());
                }
            });
            threads.add(currentThread);
            currentThread.start();
        });

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException ie) {
                throw new IllegalArgumentException(ie.getMessage());
            }
        });
    }

    public void populateRepositoryWithSerializedObjectsFromFile(String path) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(path));

        List<Person> people = (List<Person>) objectInputStream.readObject();

        for (Person person : people) {
            if (validator.validate(person)) {
                repository.add(person);
            }
        }
    }

    public void saveRepositorySerializedObjectsToFile(String path) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path));
        objectOutputStream.writeObject(repository.getAll());
    }

}