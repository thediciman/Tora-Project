package com.tora.validators;

import com.tora.entities.Person;

import java.util.regex.Pattern;

public class CNPValidator extends Validator<Person> {
    private static final Pattern CNP_REGEX = Pattern.compile("^\\d{13}$");

    public CNPValidator(Validator<Person> validator) {
        super(validator);
    }

    public boolean validate(Person person) {
        if (isValid(CNP_REGEX, person.getCNP())) {
            return super.validate(person);
        } else {
            return false;
        }
    }

}