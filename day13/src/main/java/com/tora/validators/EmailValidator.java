package com.tora.validators;

import com.tora.entities.Person;

import java.util.regex.Pattern;

public class EmailValidator extends Validator<Person> {
    private static final Pattern EMAIL_REGEX = Pattern.compile("^[\\w.]+@\\w+\\.\\w+$");

    public EmailValidator(Validator<Person> validator) {
        super(validator);
    }

    public boolean validate(Person person) {
        if (isValid(EMAIL_REGEX, person.getEmail())) {
            return super.validate(person);
        } else {
            return false;
        }
    }

}