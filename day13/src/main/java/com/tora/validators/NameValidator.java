package com.tora.validators;

import com.tora.entities.Person;

import java.util.regex.Pattern;

public class NameValidator extends Validator<Person> {
    private static final Pattern FIRST_AND_LAST_NAME_REGEX = Pattern.compile("^[A-Za-z]+$");
    private static final Pattern MIDDLE_NAME_REGEX = Pattern.compile("^(-|[A-Za-z]+)$");


    public NameValidator(Validator<Person> validator) {
        super(validator);
    }

    public boolean validate(Person person) {
        if (isValid(FIRST_AND_LAST_NAME_REGEX, person.getFirstName()) &&
                isValid(MIDDLE_NAME_REGEX, person.getMiddleName()) &&
                isValid(FIRST_AND_LAST_NAME_REGEX, person.getLastName())
        ) {
            return super.validate(person);
        } else {
            return false;
        }
    }

}
