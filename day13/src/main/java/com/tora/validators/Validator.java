package com.tora.validators;

import java.util.regex.Pattern;

public abstract class Validator<T> {
    private final Validator<T> nextValidator;

    Validator(Validator<T> validator) {
        this.nextValidator = validator;
    }

    public boolean validate(T element) {
        if (this.nextValidator != null) {
            return this.nextValidator.validate(element);
        }
        return true;
    }

    boolean isValid(Pattern pattern, String string) {
        return pattern.matcher(string).matches();
    }
}