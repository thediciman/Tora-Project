package com.tora;

import com.tora.io.IOUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Benchmarks {

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void walkCustom() {
        try {
            IOUtils.walk("day16").forEach(unused -> {
            });
        } catch (Exception ignored) {
        }
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void linesCustom() {
        try {
            IOUtils.lines("day16/test.txt").forEach(unused -> {
            });
        } catch (Exception ignored) {
        }
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void walkNIO() {
        try {
            Files.walk(Paths.get("day16")).forEach(unused -> {
            });
        } catch (Exception ignored) {
        }
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void linesNIO() {
        try {
            Files.lines(Paths.get("day16/test.txt")).forEach(unused -> {
            });
        } catch (Exception ignored) {
        }
    }

}