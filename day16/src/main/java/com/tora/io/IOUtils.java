package com.tora.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class IOUtils {

    public static Stream<String> lines(String filePath) throws IOException {
        try (
                FileInputStream fileInputStream = new FileInputStream(filePath);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            List<String> lines = new ArrayList<>();
            while (bufferedReader.ready()) {
                lines.add(bufferedReader.readLine());
            }
            return lines.stream();
        }
    }

    private static void walkRecursively(String filePath, List<Path> paths) {
        File currentFileFromPath = new File(filePath);
        paths.add(currentFileFromPath.toPath());
        if (currentFileFromPath.isDirectory()) {
            for (File fileFromDirectory : Objects.requireNonNull(currentFileFromPath.listFiles())) {
                walkRecursively(fileFromDirectory.getPath(), paths);
            }
        }
    }

    public static Stream<Path> walk(String filePath) {
        List<Path> paths = new ArrayList<>();
        walkRecursively(filePath, paths);
        return paths.stream();
    }

}