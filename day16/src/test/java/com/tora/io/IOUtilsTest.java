package com.tora.io;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class IOUtilsTest {

    @Test
    public void linesWhenGivenValidFileShouldReturnStreamOfLines() throws IOException {
        // Arrange
        String filePath = "src/test/java/com/tora/io/lines_test.in";

        // Act
        Stream<String> actual = IOUtils.lines(filePath);
        Stream<String> expected = Stream.of("line1", "line2", "line3", "line4", "line5");

        // Assert
        assertEquals(expected.collect(Collectors.toList()), actual.collect(Collectors.toList()));
    }

    @Test
    public void linesWhenGivenInvalidFileShouldThrowException() throws IOException {
        // Arrange
        String filePath = "src/test/java/com/tora/io/nope.in";

        // Act and Assert
        assertThrows(IOException.class, () -> IOUtils.lines(filePath));
    }

    @Test
    public void walkShouldReturnCorrectFiles() {
        // Arrange
        String filePath = "src/test/java/com/tora/io/walk_test_dir";

        // Act
        Stream<Path> actual = IOUtils.walk(filePath);
        Stream<Path> expected = Stream.of(
                Paths.get("src/test/java/com/tora/io/walk_test_dir"),
                Paths.get("src/test/java/com/tora/io/walk_test_dir/stuff.in")
        );

        // Assert
        assertEquals(expected.collect(Collectors.toList()), actual.collect(Collectors.toList()));
    }
}