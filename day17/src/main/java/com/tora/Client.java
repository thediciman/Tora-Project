package com.tora;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

class Client {

    private final String clientName;
    private final String sentMessagesQueueName;
    private final String receivedMessagesQueueName;
    private final Connection connection;
    private final Channel channel;
    private final DeliverCallback deliverCallback;

    private Client(Builder builder) throws IOException, TimeoutException {
        this(builder.clientName, builder.sentMessagesQueueName, builder.receivedMessagesQueueName);
    }

    Client(String clientName, String sentMessagesQueueName, String receivedMessagesQueueName) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        this.connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare(sentMessagesQueueName, false, false, false, null);
        channel.queueDeclare(receivedMessagesQueueName, false, false, false, null);

        deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println("[" + clientName + "]: Received '" + message + "'.");
        };

        this.clientName = clientName;
        this.sentMessagesQueueName = sentMessagesQueueName;
        this.receivedMessagesQueueName = receivedMessagesQueueName;
    }

    void sendMessage(String message) throws IOException {
        channel.basicPublish("", sentMessagesQueueName, null, message.getBytes(StandardCharsets.UTF_8));
        System.out.println("[" + clientName + "]: Sent '" + message + "'.");
    }

    void startReceivingMessages() throws IOException {
        channel.basicConsume(receivedMessagesQueueName, true, deliverCallback, consumerTag -> {
        });
    }

    void closeConnections() throws IOException, TimeoutException {
        channel.close();
        connection.close();
    }

    public static class Builder {
        private String clientName;
        private String sentMessagesQueueName;
        private String receivedMessagesQueueName;

        public Builder withClientName(String clientName) {
            this.clientName = clientName;
            return this;
        }

        public Builder withSentMessagesQueueName(String sentMessagesQueueName) {
            this.sentMessagesQueueName = sentMessagesQueueName;
            return this;
        }

        public Builder withReceivedMessagesQueueName(String receivedMessagesQueueName) {
            this.receivedMessagesQueueName = receivedMessagesQueueName;
            return this;
        }

        public Client build() throws IOException, TimeoutException {
            return new Client(this);
        }
    }

}