package com.tora;

public class Main {

    public static void main(String[] args) throws Exception {
        Client clientA = new Client.Builder()
                .withClientName("Client A")
                .withSentMessagesQueueName("q1")
                .withReceivedMessagesQueueName("q2")
                .build();

        Client clientB = new Client.Builder()
                .withClientName("Client B")
                .withSentMessagesQueueName("q2")
                .withReceivedMessagesQueueName("q1")
                .build();

        Client clientC = new Client.Builder()
                .withClientName("Client C")
                .withSentMessagesQueueName("q1")
                .withReceivedMessagesQueueName("q2")
                .build();

        clientB.startReceivingMessages();

        String[] stringsToSend = {"Hello", "world!"};
        for (String string : stringsToSend) {
            clientA.sendMessage(string);
            clientC.sendMessage(string.toUpperCase());
        }

        clientA.closeConnections();
        clientB.closeConnections();
        clientC.closeConnections();
    }

}