package com.tora;

import java.util.concurrent.BlockingQueue;

public class Consumer extends Thread {

    private final BlockingQueue<Integer> buffer;

    Consumer(BlockingQueue<Integer> buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        System.out.println("[CONSUMER] Started execution.");
        while (true) {
            try {
                Integer currentValue = buffer.take();
                System.out.printf("[CONSUMER] Consumed %d.\n", currentValue);
                Thread.sleep(1500);
            } catch (Exception exception) {
                System.out.println("[CONSUMER]" + exception.getMessage());
            }
        }
    }

}