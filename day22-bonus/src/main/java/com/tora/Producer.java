package com.tora;

import java.util.concurrent.BlockingQueue;

public class Producer extends Thread {

    private final BlockingQueue<Integer> buffer;

    Producer(BlockingQueue<Integer> buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        System.out.println("[PRODUCER] Started execution.");
        while (true) {
            System.out.printf("[PRODUCER] Produced %d.\n", buffer.size());
            buffer.add(buffer.size());
            try {
                Thread.sleep(500);
            } catch (Exception exception) {
                System.out.println("[PRODUCER]" + exception.getMessage());
            }
        }
    }

}