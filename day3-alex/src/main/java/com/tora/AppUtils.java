package com.tora;

class AppUtils {

    final static int ADD_LOWER_BOUND = 0;
    final static int ADD_UPPER_BOUND = Integer.MAX_VALUE / 2;

    /**
     * @param x first integer
     * @param y second integer
     * @return integer representing the sum of x and y
     * @throws IllegalArgumentException if either x or y are outside of the bounds defined above the method
     */
    static int add(int x, int y) {
        if (x < ADD_LOWER_BOUND || y < ADD_LOWER_BOUND || x > ADD_UPPER_BOUND || y > ADD_UPPER_BOUND) {
            throw new IllegalArgumentException("The parameters are outside of the allowed bounds.");
        }
        return x + y;
    }
}