package com.tora;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class AppUtilsTest {

    @Test
    public void addTestWhenGivenValidInputsShouldReturnCorrectOutput() {
        // Arrange
        int x = 1;
        int y = 2;

        // Act
        int expected = x + y;
        int actual = AppUtils.add(x, y);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void addTestWhenGivenFirstParameterIsLessThanLowerBoundShouldThrowException() {
        // Arrange
        int x = AppUtils.ADD_LOWER_BOUND - 1;
        int y = 1;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> AppUtils.add(x, y));
    }

    @Test
    public void addTestWhenGivenSecondParameterIsLessThanLowerBoundShouldThrowException() {
        // Arrange
        int x = 1;
        int y = AppUtils.ADD_LOWER_BOUND - 1;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> AppUtils.add(x, y));
    }

    @Test
    public void addTestWhenGivenFirstParameterIsGreaterThanUpperBoundShouldThrowException() {
        // Arrange
        int x = AppUtils.ADD_UPPER_BOUND + 1;
        int y = 1;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> AppUtils.add(x, y));
    }

    @Test
    public void addTestWhenGivenSecondParameterIsGreaterThanUpperBoundShouldThrowException() {
        // Arrange
        int x = 1;
        int y = AppUtils.ADD_UPPER_BOUND + 1;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> AppUtils.add(x, y));
    }

}