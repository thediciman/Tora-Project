package com.tora;

import com.tora.calculator.CLI;
import com.tora.calculator.calculator.ICalculator;
import com.tora.calculator.calculator.SimpleCalculator;

public class App {
    public static void main(String[] args) {
        ICalculator calculator = new SimpleCalculator();
        CLI cli = new CLI(calculator);
        cli.run();
    }
}