package com.tora.calculator;

import com.tora.calculator.calculator.CalculatorValidatorUtils;
import com.tora.calculator.calculator.ICalculator;
import com.tora.calculator.operation.Operation;

import java.awt.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.Scanner;

public class CLI {
    final static private String EXIT_CALCULATOR_STRING = "ok im done";
    final static private String MEME_STRING = "meme me";
    private ICalculator calculator;

    public CLI(ICalculator calculator) {
        this.calculator = calculator;
    }

    private String formatListOfOperationsIntoString(ArrayList<Operation> operations) {
        StringBuilder formattedStringBuilder = new StringBuilder();
        for (Operation operation : operations) {
            formattedStringBuilder
                    .append('\t')
                    .append(operation.getDescription())
                    .append(": ")
                    .append(operation.getExpressionFormat())
                    .append("\n");
        }
        return formattedStringBuilder.substring(0, formattedStringBuilder.length() - 1);
    }

    private void printMenu() {
        System.out.println("Welcome to the best command line calculator in the world!\n");
        System.out.println("Supported operations: \n" + formatListOfOperationsIntoString(calculator.getSupportedOperations()) + "\n");
        System.out.println("To exit the calculator, type \"" + EXIT_CALCULATOR_STRING + "\".");
        System.out.println("To get memed, type \"" + MEME_STRING + "\"!");
        System.out.println();
    }

    private void memeThatGuy() {
        try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(new URI("https://youtu.be/dQw4w9WgXcQ"));
            }
        } catch (Exception ex) {
            System.out.println("Try again later, mate. No meme for you today.");
        }
        System.out.println();
    }

    private String[] tokenizeUserInput(String userInput) {
        return userInput.trim().split("\\s+");
    }

    static Operation extractOperation(ICalculator calculator, String[] expressionTokens) {
        Operation operation = null;
        for (String token : expressionTokens) {
            if (CalculatorValidatorUtils.tokenIsOperand(token)) {
                continue;
            }
            for (Operation supportedOperation : calculator.getSupportedOperations()) {
                if (token.equals(supportedOperation.getSymbol())) {
                    if (operation == null) {
                        operation = supportedOperation;
                    } else {
                        throw new IllegalArgumentException("The expression is allowed to have a single operator.");
                    }
                }
            }
        }
        if (operation == null) {
            throw new IllegalArgumentException("The expression does not contain any operators.");
        }
        return operation;
    }

    public void run() {
        printMenu();
        Scanner scanner = new Scanner(System.in);
        String userInput;
        while (true) {
            System.out.print("Enter your expression: ");
            userInput = scanner.nextLine();
            if (userInput.equals(EXIT_CALCULATOR_STRING)) {
                break;
            }
            if (userInput.equals(MEME_STRING)) {
                memeThatGuy();
            } else {
                try {
                    String[] userInputTokens = tokenizeUserInput(userInput);
                    Operation operation = extractOperation(calculator, userInputTokens);
                    CalculatorValidatorUtils.validateUserInput(operation, userInputTokens);
                    double result = calculator.evaluateExpression(operation, userInputTokens);
                    System.out.println("The result of evaluation the given expression is: " + result + "\n");
                } catch (Exception exception) {
                    System.out.println("An error occurred:\n" + exception.getMessage() + "\n");
                }
            }
        }
        System.out.println("\nLooking forward to meeting you again!");
    }
}