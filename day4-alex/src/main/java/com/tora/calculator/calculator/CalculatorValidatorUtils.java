package com.tora.calculator.calculator;

import com.tora.calculator.operation.Operation;

import java.util.ArrayList;

public class CalculatorValidatorUtils {
    public static boolean tokenIsOperator(ICalculator calculator, String token) {
        ArrayList<Operation> operations = calculator.getSupportedOperations();
        for (Operation operation : operations) {
            if (token.equals(operation.getSymbol())) {
                return true;
            }
        }
        return false;
    }

    public static boolean tokenIsOperand(String token) {
        if (token.matches(".*[a-zA-Z].*")) {
            return false;
        }
        try {
            Double.parseDouble(token);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static void validateUserInput(Operation operation, String[] userInputTokens) {
        for (String token : userInputTokens) {
            if (!token.equals(operation.getSymbol()) && !tokenIsOperand(token)) {
                throw new IllegalArgumentException("Token '" + token + "' is not an operand.");
            }
        }
        operation.validateExpression(userInputTokens);
    }

    public static void validateExpressionForBinaryOperationWithMiddleOperator(Operation operation, String[] expressionTokens) {
        StringBuilder exceptionMessage = new StringBuilder();
        if (expressionTokens.length != 3) {
            exceptionMessage.append("The expression should contain three tokens.\n");
        }
        if (expressionTokens.length > 0 && !tokenIsOperand(expressionTokens[0])) {
            exceptionMessage.append("The first token should be an operand.\n");
        }
        if (expressionTokens.length > 1 && !expressionTokens[1].equals(operation.getSymbol())) {
            exceptionMessage.append("The second token should be an operator.\n");
        }
        if (expressionTokens.length > 2 && !tokenIsOperand(expressionTokens[2])) {
            exceptionMessage.append("The third token should be an operand.\n");
        }
        if (exceptionMessage.length() != 0) {
            throw new IllegalArgumentException(exceptionMessage.substring(0, exceptionMessage.length() - 1));
        }
    }

    public static void validateExpressionForBinaryOperationWithFrontOperator(Operation operation, String[] expressionTokens) {
        StringBuilder exceptionMessage = new StringBuilder();
        if (expressionTokens.length != 3) {
            exceptionMessage.append("The expression should contain three tokens.\n");
        }
        if (expressionTokens.length > 0 && !expressionTokens[0].equals(operation.getSymbol())) {
            exceptionMessage.append("The first token should be an operator.\n");
        }
        if (expressionTokens.length > 1 && !tokenIsOperand(expressionTokens[1])) {
            exceptionMessage.append("The second token should be an operand.\n");
        }
        if (expressionTokens.length > 2 && !tokenIsOperand(expressionTokens[2])) {
            exceptionMessage.append("The third token should be an operand.\n");
        }
        if (exceptionMessage.length() != 0) {
            throw new IllegalArgumentException(exceptionMessage.substring(0, exceptionMessage.length() - 1));
        }
    }

    public static void validateExpressionForUnaryOperation(Operation operation, String[] expressionTokens) {
        StringBuilder exceptionMessage = new StringBuilder();
        if (expressionTokens.length != 2) {
            exceptionMessage.append("The expression should contain two tokens.\n");
        }
        if (expressionTokens.length > 0 && !expressionTokens[0].equals(operation.getSymbol())) {
            exceptionMessage.append("The first token should be an operator.\n");
        }
        if (expressionTokens.length > 1 && !tokenIsOperand(expressionTokens[1])) {
            exceptionMessage.append("The second token should be an operand.\n");
        }
        if (exceptionMessage.length() != 0) {
            throw new IllegalArgumentException(exceptionMessage.substring(0, exceptionMessage.length() - 1));
        }
    }
}