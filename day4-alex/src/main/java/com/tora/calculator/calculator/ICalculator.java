package com.tora.calculator.calculator;

import com.tora.calculator.operation.Operation;

import java.util.ArrayList;

public interface ICalculator {
    ArrayList<Operation> getSupportedOperations();

    double evaluateExpression(Operation operation, String[] expressionTokens);
}