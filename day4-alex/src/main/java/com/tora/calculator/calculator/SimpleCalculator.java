package com.tora.calculator.calculator;

import com.tora.calculator.operation.Operation;
import com.tora.calculator.operation.operations.Addition;
import com.tora.calculator.operation.operations.Ceil;
import com.tora.calculator.operation.operations.Cosinus;
import com.tora.calculator.operation.operations.Division;
import com.tora.calculator.operation.operations.Exponentiation;
import com.tora.calculator.operation.operations.Floor;
import com.tora.calculator.operation.operations.LogarithmInBase10;
import com.tora.calculator.operation.operations.LogarithmInBase2;
import com.tora.calculator.operation.operations.Maximum;
import com.tora.calculator.operation.operations.Minimum;
import com.tora.calculator.operation.operations.Multiplication;
import com.tora.calculator.operation.operations.NaturalLogarithm;
import com.tora.calculator.operation.operations.PowerOf;
import com.tora.calculator.operation.operations.Round;
import com.tora.calculator.operation.operations.Sinus;
import com.tora.calculator.operation.operations.SquareRoot;
import com.tora.calculator.operation.operations.Subtraction;
import com.tora.calculator.operation.operations.Tangent;

import java.util.ArrayList;

public class SimpleCalculator implements ICalculator {

    private ArrayList<Operation> supportedOperations = new ArrayList<Operation>() {{
        add(new Addition());
        add(new Subtraction());
        add(new Division());
        add(new Multiplication());
        add(new Exponentiation());
        add(new NaturalLogarithm());
        add(new LogarithmInBase2());
        add(new LogarithmInBase10());
        add(new SquareRoot());
        add(new Sinus());
        add(new Cosinus());
        add(new Tangent());
        add(new Floor());
        add(new Ceil());
        add(new Minimum());
        add(new Maximum());
        add(new Round());
        add(new PowerOf());
    }};

    @Override
    public ArrayList<Operation> getSupportedOperations() {
        return supportedOperations;
    }

    @Override
    public double evaluateExpression(Operation operation, String[] expressionTokens) {
        ArrayList<Double> operands = operation.getOperands(expressionTokens);
        return operation.performOperation(operands.toArray(new Double[0]));
    }
}