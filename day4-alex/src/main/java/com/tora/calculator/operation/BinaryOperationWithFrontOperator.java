package com.tora.calculator.operation;

import com.tora.calculator.calculator.CalculatorValidatorUtils;

import java.util.ArrayList;

public abstract class BinaryOperationWithFrontOperator extends Operation {
    public BinaryOperationWithFrontOperator(String description, String symbol) {
        super(description, symbol);
    }

    public void validateExpression(String[] tokens) {
        CalculatorValidatorUtils.validateExpressionForBinaryOperationWithFrontOperator(this, tokens);
    }

    public String getExpressionFormat() {
        return this.getSymbol() + " <first operand> <second operand>";
    }

    public ArrayList<Double> getOperands(String[] tokens) {
        return new ArrayList<Double>() {{
            add(Double.valueOf(tokens[1]));
            add(Double.valueOf(tokens[2]));
        }};
    }
}