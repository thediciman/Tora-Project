package com.tora.calculator.operation;

import com.tora.calculator.calculator.CalculatorValidatorUtils;

import java.util.ArrayList;

public abstract class BinaryOperationWithMiddleOperator extends Operation {
    public BinaryOperationWithMiddleOperator(String description, String symbol) {
        super(description, symbol);
    }

    public void validateExpression(String[] tokens) {
        CalculatorValidatorUtils.validateExpressionForBinaryOperationWithMiddleOperator(this, tokens);
    }

    public String getExpressionFormat() {
        return "<first operand> " + this.getSymbol() + " <second operand>";
    }

    public ArrayList<Double> getOperands(String[] tokens) {
        return new ArrayList<Double>() {{
            add(Double.valueOf(tokens[0]));
            add(Double.valueOf(tokens[2]));
        }};
    }
}