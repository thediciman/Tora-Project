package com.tora.calculator.operation;

import java.util.ArrayList;

public abstract class Operation {
    private String description;
    private String symbol;

    public Operation(String description, String symbol) {
        this.description = description;
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public String getSymbol() {
        return symbol;
    }

    public abstract String getExpressionFormat();

    public abstract void validateExpression(String[] tokens);

    public abstract double performOperation(Double... operands);

    public abstract ArrayList<Double> getOperands(String[] tokens);
}