package com.tora.calculator.operation;

import com.tora.calculator.calculator.CalculatorValidatorUtils;

import java.util.ArrayList;

public abstract class UnaryOperation extends Operation {
    public UnaryOperation(String description, String symbol) {
        super(description, symbol);
    }

    public void validateExpression(String[] tokens) {
        CalculatorValidatorUtils.validateExpressionForUnaryOperation(this, tokens);
    }

    public String getExpressionFormat() {
        return this.getSymbol() + " <operand>";
    }

    public ArrayList<Double> getOperands(String[] tokens) {
        return new ArrayList<Double>() {{
            add(Double.valueOf(tokens[1]));
        }};
    }
}