package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithMiddleOperator;

public class Addition extends BinaryOperationWithMiddleOperator {
    public Addition() {
        super("Addition", "+");
    }

    public double performOperation(Double... operands) {
        return operands[0] + operands[1];
    }
}