package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class Ceil extends UnaryOperation {
    public Ceil() {
        super("Ceil", "ceil");
    }

    public double performOperation(Double... operands) {
        return Math.ceil(operands[0]);
    }
}