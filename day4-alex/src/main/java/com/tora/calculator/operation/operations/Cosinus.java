package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class Cosinus extends UnaryOperation {
    public Cosinus() {
        super("Cosinus", "cos");
    }

    public double performOperation(Double... operands) {
        return Math.cos(operands[0]);
    }
}