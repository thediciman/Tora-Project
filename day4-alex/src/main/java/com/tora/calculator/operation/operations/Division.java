package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithMiddleOperator;

public class Division extends BinaryOperationWithMiddleOperator {
    public Division() {
        super("Division", "/");
    }

    // extract validation
    public double performOperation(Double... operands) {
        if (operands[1] == 0) {
            throw new IllegalArgumentException("Can't perform division by 0.");
        }
        return operands[0] / operands[1];
    }
}