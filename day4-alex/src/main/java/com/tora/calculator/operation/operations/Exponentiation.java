package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithMiddleOperator;

public class Exponentiation extends BinaryOperationWithMiddleOperator {
    public Exponentiation() {
        super("Exponentiation", "^");
    }

    public double performOperation(Double... operands) {
        return Math.pow(operands[0], operands[1]);
    }
}