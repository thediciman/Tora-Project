package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class Floor extends UnaryOperation {
    public Floor() {
        super("Floor", "floor");
    }

    public double performOperation(Double... operands) {
        return Math.floor(operands[0]);
    }
}