package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class LogarithmInBase10 extends UnaryOperation {
    public LogarithmInBase10() {
        super("Logarithm in base 10", "log10");
    }

    public double performOperation(Double... operands) {
        if (operands[0] <= 0) {
            throw new IllegalArgumentException("The operand should be greater than 0.");
        }
        return Math.log10(operands[0]);
    }
}