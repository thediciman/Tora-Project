package com.tora.calculator.operation.operations;

import com.google.common.math.DoubleMath;
import com.tora.calculator.operation.UnaryOperation;

public class LogarithmInBase2 extends UnaryOperation {
    public LogarithmInBase2() {
        super("Logarithm in base 2", "log2");
    }

    public double performOperation(Double... operands) {
        if (operands[0] <= 0) {
            throw new IllegalArgumentException("The operand should be greater than 0.");
        }
        return DoubleMath.log2(operands[0]);
    }
}