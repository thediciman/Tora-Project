package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithFrontOperator;

public class Maximum extends BinaryOperationWithFrontOperator {
    public Maximum() {
        super("Maximum", "max");
    }

    public double performOperation(Double... operands) {
        return Math.max(operands[0], operands[1]);
    }
}