package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithFrontOperator;

public class Minimum extends BinaryOperationWithFrontOperator {
    public Minimum() {
        super("Minimum", "min");
    }

    public double performOperation(Double... operands) {
        return Math.min(operands[0], operands[1]);
    }
}