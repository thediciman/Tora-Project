package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithMiddleOperator;

public class Multiplication extends BinaryOperationWithMiddleOperator {
    public Multiplication() {
        super("Multiplication", "*");
    }

    public double performOperation(Double... operands) {
        return operands[0] * operands[1];
    }
}