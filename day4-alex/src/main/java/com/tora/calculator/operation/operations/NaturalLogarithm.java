package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class NaturalLogarithm extends UnaryOperation {
    public NaturalLogarithm() {
        super("Natural logarithm", "ln");
    }

    public double performOperation(Double... operands) {
        if (operands[0] <= 0) {
            throw new IllegalArgumentException("The operand should be greater than 0.");
        }
        return Math.log(operands[0]);
    }
}