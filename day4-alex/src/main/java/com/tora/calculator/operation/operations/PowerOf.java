package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithFrontOperator;

public class PowerOf extends BinaryOperationWithFrontOperator {
    public PowerOf() {
        super("Power of", "powerOf");
    }

    public double performOperation(Double... operands) {
        return Math.pow(operands[0], operands[1]);
    }
}