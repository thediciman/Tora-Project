package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class Round extends UnaryOperation {
    public Round() {
        super("Round", "round");
    }

    public double performOperation(Double... operands) {
        return Math.round(operands[0]);
    }
}