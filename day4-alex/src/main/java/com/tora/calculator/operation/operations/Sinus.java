package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class Sinus extends UnaryOperation {
    public Sinus() {
        super("Sinus", "sin");
    }

    public double performOperation(Double... operands) {
        return Math.sin(operands[0]);
    }
}