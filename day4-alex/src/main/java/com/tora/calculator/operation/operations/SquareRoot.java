package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class SquareRoot extends UnaryOperation {
    public SquareRoot() {
        super("Square root", "sqrt");
    }

    public double performOperation(Double... operands) {
        if (operands[0] < 0) {
            throw new IllegalArgumentException("The operand should be greater than or equal to 0.");
        }
        return Math.sqrt(operands[0]);
    }
}