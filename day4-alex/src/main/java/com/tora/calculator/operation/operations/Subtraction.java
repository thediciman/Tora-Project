package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithMiddleOperator;

public class Subtraction extends BinaryOperationWithMiddleOperator {
    public Subtraction() {
        super("Subtraction", "-");
    }

    public double performOperation(Double... operands) {
        return operands[0] - operands[1];
    }
}