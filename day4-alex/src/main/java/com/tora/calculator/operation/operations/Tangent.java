package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperation;

public class Tangent extends UnaryOperation {
    public Tangent() {
        super("Tangent", "tan");
    }

    public double performOperation(Double... operands) {
        return Math.tan(operands[0]);
    }
}