package com.tora.calculator;

import com.tora.calculator.calculator.CalculatorValidatorUtils;
import com.tora.calculator.calculator.SimpleCalculator;
import com.tora.calculator.operation.Operation;
import com.tora.calculator.operation.operations.Addition;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class CalculatorValidatorUtilsTest {

    @Test
    public void tokenIsOperatorWhenGivenValidTokenForSimpleCalculatorShouldReturnTrue() {
        // Arrange
        SimpleCalculator calculator = new SimpleCalculator();
        String token = "+";

        // Act
        boolean actual = CalculatorValidatorUtils.tokenIsOperator(calculator, token);

        // Assert
        assertTrue(actual);
    }

    @Test
    public void tokenIsOperatorWhenGivenInvalidTokenForSimpleCalculatorShouldReturnTrue() {
        // Arrange
        SimpleCalculator calculator = new SimpleCalculator();
        String token = "not an operator";

        // Act
        boolean actual = CalculatorValidatorUtils.tokenIsOperator(calculator, token);

        // Assert
        assertFalse(actual);
    }

    @Test
    public void tokenIsOperandWhenGivenTokenIsValidShouldReturnTrue() {
        // Arrange
        String token = "10.33";

        // Act
        boolean actual = CalculatorValidatorUtils.tokenIsOperand(token);

        // Assert
        assertTrue(actual);
    }

    @Test
    public void tokenIsOperandWhenGivenTokenIsInvalidShouldReturnFalse() {
        // Arrange
        String token = "10a.33";

        // Act
        boolean actual = CalculatorValidatorUtils.tokenIsOperand(token);

        // Assert
        assertFalse(actual);
    }

    @Test
    public void validateUserInputWhenGivenValidInputShouldNotThrowException() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {"1", "+", "2"};

        // Act and Assert
        try {
            CalculatorValidatorUtils.validateUserInput(operation, tokens);
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    public void validateUserInputWhenGivenTokenWhichIsNotOperandShouldThrowException() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {"1d", "+", "2"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> CalculatorValidatorUtils.validateUserInput(operation, tokens));
    }
}