package com.tora.calculator;

import com.tora.calculator.calculator.SimpleCalculator;
import com.tora.calculator.operation.Operation;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SimpleCalculatorTest {

    private SimpleCalculator calculator = new SimpleCalculator();

    @Test
    public void getSupportedOperationsShouldReturnNonEmptyArrayOfOperations() {
        // Act
        ArrayList<Operation> operations = calculator.getSupportedOperations();

        // Assert
        assertTrue(operations.size() > 0);
    }

    @Test
    public void evaluateExpressionWhenGivenExpressionWithBinaryOperatorShouldEvaluateAndReturnTheCorrectResultOfTheExpression() {
        // Arrange
        String[] expressionTokens = {"2", "^", "10"};

        // Act
        double expected = Math.pow(2, 10);
        double actual = calculator.evaluateExpression(CLI.extractOperation(calculator, expressionTokens), expressionTokens);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void evaluateExpressionWhenGivenExpressionWithUnaryOperatorShouldEvaluateAndReturnTheCorrectResultOfTheExpression() {
        // Arrange
        String[] expressionTokens = {"log10", "1000000000"};

        // Act
        double expected = Math.log10(1000000000);
        double actual = calculator.evaluateExpression(CLI.extractOperation(calculator, expressionTokens), expressionTokens);

        // Assert
        assertEquals(expected, actual, 0);
    }
}