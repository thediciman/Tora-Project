package com.tora.calculator.operation;

import com.tora.calculator.operation.operations.Maximum;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;

public class BinaryOperationWithFrontOperatorTest {
    @Test
    public void getExpressionFormatShouldReturnCorrectFormat() {
        // Arrange
        Operation operation = new Maximum();

        // Act
        String expected = operation.getSymbol() + " <first operand> <second operand>";
        String actual = operation.getExpressionFormat();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void validateExpressionWhenInvalidNumberOfTokensShouldThrowException() {
        // Arrange
        Operation operation = new Maximum();
        String[] tokens = {};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenFirstTokenIsNotAnOperatorShouldThrowException() {
        // Arrange
        Operation operation = new Maximum();
        String[] tokens = {"1"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenSecondTokenIsNotAnOperatorShouldThrowException() {
        // Arrange
        Operation operation = new Maximum();
        String[] tokens = {"max", "max"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenThirdTokenIsNotAnOperatorShouldThrowException() {
        // Arrange
        Operation operation = new Maximum();
        String[] tokens = {"max", "1", "max"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenInputIsValidShouldNotThrowException() {
        // Arrange
        Operation operation = new Maximum();
        String[] tokens = {"max", "1", "2"};

        // Act and Assert
        try {
            operation.validateExpression(tokens);
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    public void getOperandsShouldReturnTheCorrectOperands() {
        // Arrange
        Operation operation = new Maximum();
        String[] tokens = {"max", "1", "2"};

        // Act
        ArrayList<Double> expected = new ArrayList<Double>() {{
            add(1d);
            add(2d);
        }};
        ArrayList<Double> actual = operation.getOperands(tokens);

        // Assert
        assertEquals(expected, actual);
    }
}