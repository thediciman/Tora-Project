package com.tora.calculator.operation;

import com.tora.calculator.operation.operations.Addition;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;

public class BinaryOperationWithMiddleOperatorTest {
    @Test
    public void getExpressionFormatShouldReturnCorrectFormat() {
        // Arrange
        Operation operation = new Addition();

        // Act
        String expected = "<first operand> " + operation.getSymbol() + " <second operand>";
        String actual = operation.getExpressionFormat();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void validateExpressionWhenInvalidNumberOfTokensShouldThrowException() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenFirstTokenIsNotAnOperandShouldThrowException() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {"+"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenSecondTokenIsNotAnOperatorShouldThrowException() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {"1", "1"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenThirdTokenIsNotAnOperandShouldThrowException() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {"1", "+", "+"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenInputIsValidShouldNotThrowException() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {"2", "+", "2"};

        // Act and Assert
        try {
            operation.validateExpression(tokens);
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    public void getOperandsShouldReturnTheCorrectOperands() {
        // Arrange
        Operation operation = new Addition();
        String[] tokens = {"1", "+", "2"};

        // Act
        ArrayList<Double> expected = new ArrayList<Double>() {{
            add(1d);
            add(2d);
        }};
        ArrayList<Double> actual = operation.getOperands(tokens);

        // Assert
        assertEquals(expected, actual);
    }
}