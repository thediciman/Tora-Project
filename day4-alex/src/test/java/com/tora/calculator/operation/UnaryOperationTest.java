package com.tora.calculator.operation;

import com.tora.calculator.operation.operations.SquareRoot;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;

public class UnaryOperationTest {
    @Test
    public void getExpressionFormatShouldReturnCorrectFormat() {
        // Arrange
        Operation operation = new SquareRoot();

        // Act
        String expected = operation.getSymbol() + " <operand>";
        String actual = operation.getExpressionFormat();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void validateExpressionWhenInvalidNumberOfTokensShouldThrowException() {
        // Arrange
        Operation operation = new SquareRoot();
        String[] tokens = {};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenFirstTokenIsNotAnOperatorShouldThrowException() {
        // Arrange
        Operation operation = new SquareRoot();
        String[] tokens = {"69420"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenSecondTokenIsNotAnOperandShouldThrowException() {
        // Arrange
        Operation operation = new SquareRoot();
        String[] tokens = {"sqrt", "sqrt"};

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> operation.validateExpression(tokens));
    }

    @Test
    public void validateExpressionWhenInputIsValidShouldNotThrowException() {
        // Arrange
        Operation operation = new SquareRoot();
        String[] tokens = {"sqrt", "2"};

        // Act and Assert
        try {
            operation.validateExpression(tokens);
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    public void getOperandsShouldReturnTheCorrectOperands() {
        // Arrange
        Operation operation = new SquareRoot();
        String[] tokens = {"sqrt", "2"};

        // Act
        ArrayList<Double> expected = new ArrayList<Double>() {{
            add(2d);
        }};
        ArrayList<Double> actual = operation.getOperands(tokens);

        // Assert
        assertEquals(expected, actual);
    }
}