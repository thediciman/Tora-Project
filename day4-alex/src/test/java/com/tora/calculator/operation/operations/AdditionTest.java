package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AdditionTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 33.66;

        // Act
        double expected = firstOperand + secondOperand;
        double actual = new Addition().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Addition addition = new Addition();

        // Act
        String expected = "+";
        String actual = addition.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Addition addition = new Addition();

        // Act
        String expected = "Addition";
        String actual = addition.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}