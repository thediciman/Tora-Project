package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CeilTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.ceil(operand);
        double actual = new Ceil().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Ceil ceil = new Ceil();

        // Act
        String expected = "ceil";
        String actual = ceil.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Ceil ceil = new Ceil();

        // Act
        String expected = "Ceil";
        String actual = ceil.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}