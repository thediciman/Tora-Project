package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CosinusTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.cos(operand);
        double actual = new Cosinus().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Cosinus cosinus = new Cosinus();

        // Act
        String expected = "cos";
        String actual = cosinus.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Cosinus cosinus = new Cosinus();

        // Act
        String expected = "Cosinus";
        String actual = cosinus.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}