package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class DivisionTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 33.66;

        // Act
        double expected = firstOperand / secondOperand;
        double actual = new Division().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void performOperationWhenGivenZeroAsSecondOperandShouldThrowException() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 0;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> new Division().performOperation(firstOperand, secondOperand));
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Division division = new Division();

        // Act
        String expected = "/";
        String actual = division.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Division division = new Division();

        // Act
        String expected = "Division";
        String actual = division.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}