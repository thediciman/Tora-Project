package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExponentiationTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 5.66;

        // Act
        double expected = Math.pow(firstOperand, secondOperand);
        double actual = new Exponentiation().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Exponentiation exponentiation = new Exponentiation();

        // Act
        String expected = "^";
        String actual = exponentiation.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Exponentiation exponentiation = new Exponentiation();

        // Act
        String expected = "Exponentiation";
        String actual = exponentiation.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}