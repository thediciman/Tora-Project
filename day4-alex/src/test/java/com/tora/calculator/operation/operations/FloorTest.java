package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FloorTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.floor(operand);
        double actual = new Floor().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Floor floor = new Floor();

        // Act
        String expected = "floor";
        String actual = floor.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Floor floor = new Floor();

        // Act
        String expected = "Floor";
        String actual = floor.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}