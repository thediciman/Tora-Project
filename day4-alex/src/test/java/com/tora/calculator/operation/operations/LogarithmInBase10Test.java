package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class LogarithmInBase10Test {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.log10(operand);
        double actual = new LogarithmInBase10().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void performOperationWhenOperandLessThanOrEqualToZeroShouldThrowException() {
        // Arrange
        double operand = 0;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> new LogarithmInBase10().performOperation(operand));
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        LogarithmInBase10 logarithmInBase10 = new LogarithmInBase10();

        // Act
        String expected = "log10";
        String actual = logarithmInBase10.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        LogarithmInBase10 logarithmInBase10 = new LogarithmInBase10();

        // Act
        String expected = "Logarithm in base 10";
        String actual = logarithmInBase10.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}