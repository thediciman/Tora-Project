package com.tora.calculator.operation.operations;

import com.google.common.math.DoubleMath;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class LogarithmInBase2Test {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = DoubleMath.log2(operand);
        double actual = new LogarithmInBase2().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void performOperationWhenOperandLessThanOrEqualToZeroShouldThrowException() {
        // Arrange
        double operand = 0;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> new LogarithmInBase2().performOperation(operand));
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        LogarithmInBase2 logarithmInBase2 = new LogarithmInBase2();

        // Act
        String expected = "log2";
        String actual = logarithmInBase2.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        LogarithmInBase10 logarithmInBase2 = new LogarithmInBase10();

        // Act
        String expected = "Logarithm in base 10";
        String actual = logarithmInBase2.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}