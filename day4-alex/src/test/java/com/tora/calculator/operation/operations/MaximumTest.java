package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MaximumTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 33.66;

        // Act
        double expected = Math.max(firstOperand, secondOperand);
        double actual = new Maximum().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Maximum maximum = new Maximum();

        // Act
        String expected = "max";
        String actual = maximum.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Maximum maximum = new Maximum();

        // Act
        String expected = "Maximum";
        String actual = maximum.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}