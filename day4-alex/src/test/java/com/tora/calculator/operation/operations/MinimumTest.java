package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MinimumTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 33.66;

        // Act
        double expected = Math.min(firstOperand, secondOperand);
        double actual = new Minimum().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Minimum Minimum = new Minimum();

        // Act
        String expected = "min";
        String actual = Minimum.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Minimum Minimum = new Minimum();

        // Act
        String expected = "Minimum";
        String actual = Minimum.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}