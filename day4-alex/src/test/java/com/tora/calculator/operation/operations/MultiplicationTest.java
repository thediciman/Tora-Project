package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MultiplicationTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 33.66;

        // Act
        double expected = firstOperand * secondOperand;
        double actual = new Multiplication().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Multiplication multiplication = new Multiplication();

        // Act
        String expected = "*";
        String actual = multiplication.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Multiplication multiplication = new Multiplication();

        // Act
        String expected = "Multiplication";
        String actual = multiplication.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}