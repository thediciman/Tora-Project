package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class NaturalLogarithmTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.log(operand);
        double actual = new NaturalLogarithm().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void performOperationWhenOperandLessThanOrEqualToZeroShouldThrowException() {
        // Arrange
        double operand = 0;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> new NaturalLogarithm().performOperation(operand));
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        NaturalLogarithm naturalLogarithm = new NaturalLogarithm();

        // Act
        String expected = "ln";
        String actual = naturalLogarithm.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        NaturalLogarithm naturalLogarithm = new NaturalLogarithm();

        // Act
        String expected = "Natural logarithm";
        String actual = naturalLogarithm.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}