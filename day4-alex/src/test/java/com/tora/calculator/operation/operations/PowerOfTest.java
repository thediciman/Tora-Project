package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PowerOfTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 33.66;

        // Act
        double expected = Math.pow(firstOperand, secondOperand);
        double actual = new PowerOf().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        PowerOf PowerOf = new PowerOf();

        // Act
        String expected = "powerOf";
        String actual = PowerOf.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        PowerOf PowerOf = new PowerOf();

        // Act
        String expected = "Power of";
        String actual = PowerOf.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}