package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SinusTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.sin(operand);
        double actual = new Sinus().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Sinus sinus = new Sinus();

        // Act
        String expected = "sin";
        String actual = sinus.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Sinus sinus = new Sinus();

        // Act
        String expected = "Sinus";
        String actual = sinus.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}