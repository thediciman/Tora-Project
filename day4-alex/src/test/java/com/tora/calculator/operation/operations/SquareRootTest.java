package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class SquareRootTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.sqrt(operand);
        double actual = new SquareRoot().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void performOperationWhenOperandLessThanZeroShouldThrowException() {
        // Arrange
        double operand = -0.00001;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> new SquareRoot().performOperation(operand));
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        SquareRoot squareRoot = new SquareRoot();

        // Act
        String expected = "sqrt";
        String actual = squareRoot.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        SquareRoot squareRoot = new SquareRoot();

        // Act
        String expected = "Square root";
        String actual = squareRoot.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}