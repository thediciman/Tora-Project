package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubtractionTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double firstOperand = 10.35;
        double secondOperand = 33.66;

        // Act
        double expected = firstOperand - secondOperand;
        double actual = new Subtraction().performOperation(firstOperand, secondOperand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Subtraction subtraction = new Subtraction();

        // Act
        String expected = "-";
        String actual = subtraction.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Subtraction subtraction = new Subtraction();

        // Act
        String expected = "Subtraction";
        String actual = subtraction.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}