package com.tora.calculator.operation.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TangentTest {
    @Test
    public void performOperationWhenGivenValidOperandsShouldReturnCorrectResult() {
        // Arrange
        double operand = 10.35;

        // Act
        double expected = Math.tan(operand);
        double actual = new Tangent().performOperation(operand);

        // Assert
        assertEquals(expected, actual, 0);
    }

    @Test
    public void getSymbolShouldReturnCorrectSymbol() {
        // Arrange
        Tangent tangent = new Tangent();

        // Act
        String expected = "tan";
        String actual = tangent.getSymbol();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDescriptionShouldReturnCorrectSymbol() {
        // Arrange
        Tangent tangent = new Tangent();

        // Act
        String expected = "Tangent";
        String actual = tangent.getDescription();

        // Assert
        assertEquals(expected, actual);
    }
}