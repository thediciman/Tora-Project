package com.tora;

import com.tora.multimap.multimapv2.MultiMapKey;
import com.tora.multimap.multimapv2.MultiMapV2;

public class App {
    public static void main(String[] args) {
        MultiMapV2<String> multiMap = new MultiMapV2<>();

        multiMap.put(new MultiMapKey("south"), "Very sensitive data");

        System.out.println(multiMap.get(new MultiMapKey()));

        System.out.println(multiMap.keySet());
        System.out.println(multiMap.values());
        System.out.println(multiMap.entrySet());
    }
}