package com.tora.multimap.multimapv1;

import javax.annotation.Nonnull;
import javax.management.openmbean.InvalidKeyException;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MultiMapV1<K, V> implements Map<K, V> {

    private Map<V, Set<K>> container = new HashMap<>();
    private int numberOfEntries = 0;

    @Override
    public int size() {
        return numberOfEntries;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return container.values()
                .stream()
                .anyMatch(keyList -> keyList.contains(key));
    }

    @Override
    public boolean containsValue(Object value) {
        return container.keySet()
                .stream()
                .anyMatch(containerValue -> containerValue.equals(value));
    }

    @Override
    public V get(Object key) {
        return container.entrySet()
                .stream()
                .filter(entry -> entry.getValue().contains(key))
                .findFirst()
                .orElseThrow(InvalidKeyException::new)
                .getKey();
    }

    @Override
    public V put(K key, V value) {
        container.computeIfAbsent(value, k -> new HashSet<>());
        Set<K> setOfKeys = container.get(value);
        if (setOfKeys.add(key)) {
            ++numberOfEntries;
        }
        container.put(value, setOfKeys);
        return value;
    }

    @Override
    public V remove(Object key) {
        V value = get(key);
        numberOfEntries -= container.get(value).size();
        container.remove(value);
        return value;
    }

    @Override
    public void putAll(@Nonnull Map<? extends K, ? extends V> m) {
        m.forEach(this::put);
    }

    @Override
    public void clear() {
        container.clear();
        numberOfEntries = 0;
    }

    @Override
    @Nonnull
    public Set<K> keySet() {
        return container.values()
                .stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    @Override
    @Nonnull
    public Collection<V> values() {
        return container.keySet();
    }

    @Override
    @Nonnull
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> resultedSet = new HashSet<>();
        container.forEach((value, keys) -> keys.forEach(key -> resultedSet.add(new AbstractMap.SimpleEntry<>(key, value))));
        return resultedSet;
    }
}