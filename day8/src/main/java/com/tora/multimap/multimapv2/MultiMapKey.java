package com.tora.multimap.multimapv2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

final public class MultiMapKey {
    final private List<Object> subKeys;

    public MultiMapKey(Object... subKeys) {
        this.subKeys = new ArrayList<>(Arrays.asList(subKeys));
    }

    @Override
    public String toString() {
        return "MultiMapKey{" + subKeys + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MultiMapKey that = (MultiMapKey) o;
        return Objects.equals(subKeys, that.subKeys);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subKeys);
    }
}