package com.tora.multimap.multimapv2;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MultiMapV2<V> implements Map<MultiMapKey, V> {

    final private Map<MultiMapKey, V> container;

    public MultiMapV2() {
        container = new HashMap<>();
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public boolean isEmpty() {
        return container.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return container.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return container.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return container.get(key);
    }

    @Override
    public V put(MultiMapKey key, V value) {
        return container.put(key, value);
    }

    @Override
    public V remove(Object key) {
        return container.remove(key);
    }

    @Override
    public void putAll(@Nonnull Map<? extends MultiMapKey, ? extends V> m) {
        container.putAll(m);
    }

    @Override
    public void clear() {
        container.clear();
    }

    @Override
    @Nonnull
    public Set<MultiMapKey> keySet() {
        return container.keySet();
    }

    @Override
    @Nonnull
    public Collection<V> values() {
        return container.values();
    }

    @Override
    @Nonnull
    public Set<Entry<MultiMapKey, V>> entrySet() {
        return container.entrySet();
    }
}