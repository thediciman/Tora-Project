package org.tora.benchmark;

import org.tora.entities.Order;
import org.tora.repositories.InMemoryRepository;

import java.util.Random;
import java.util.stream.IntStream;

public class BenchmarkState {
    final public int NUMBER_OF_ELEMENTS = 5000000;
    public Random random;
    public InMemoryRepository<Order> repository;
    public int currentItem;

    public void setUp(InMemoryRepository<Order> inMemoryRepository) {
        currentItem = 0;
        random = new Random();
        repository = inMemoryRepository;
        IntStream.range(0, NUMBER_OF_ELEMENTS).forEach(
                value -> repository.add(new Order.Builder()
                        .withID(value)
                        .withPrice(value)
                        .withQuantity(value)
                        .build()
                )
        );
    }

    public void tearDown() {
        System.out.println(repository.size() + " elements at tearDown.");
    }
}
