package org.tora.benchmark.benchmarks;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.tora.benchmark.BenchmarkState;
import org.tora.entities.Order;
import org.tora.repositories.implementations.fastutil.FastutilListBasedRepository;

import java.util.concurrent.TimeUnit;

public class FastutilListRepositoryBenchmark {

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void benchmarkAddForFastutilListBasedRepository(FastutilListBasedRepositoryState state) {
        int value = state.currentItem + state.NUMBER_OF_ELEMENTS;
        state.repository.add(new Order.Builder()
                .withID(value)
                .withPrice(value)
                .withQuantity(value)
                .build());
        ++state.currentItem;
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void benchmarkContainsForFastutilListBasedRepository(FastutilListBasedRepositoryState state) {
        int value = state.random.nextInt(state.NUMBER_OF_ELEMENTS * 2);
        state.repository.add(
                new Order.Builder()
                        .withID(value)
                        .withPrice(value)
                        .withQuantity(value)
                        .build());
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void benchmarkRemoveForFastutilListBasedRepository(FastutilListBasedRepositoryState state) {
        int value = state.random.nextInt(state.NUMBER_OF_ELEMENTS);
        state.repository.remove(new Order.Builder()
                .withID(value)
                .withPrice(value)
                .withQuantity(value)
                .build());
    }

    @State(Scope.Thread)
    public static class FastutilListBasedRepositoryState extends BenchmarkState {
        @Setup(Level.Iteration)
        public void setUp() {
            super.setUp(new FastutilListBasedRepository<>());
        }

        @TearDown(Level.Iteration)
        public void tearDown() {
            super.tearDown();
        }
    }

}