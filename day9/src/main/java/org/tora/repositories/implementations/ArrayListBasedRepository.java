package org.tora.repositories.implementations;

import org.tora.repositories.InMemoryRepository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private final List<T> list;

    public ArrayListBasedRepository() {
        list = new ArrayList<>();
    }

    @Override
    public void add(T element) {
        list.add(element);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }

    @Override
    public int size() {
        return list.size();
    }
}