package org.tora.repositories.implementations.eclipse;

import org.eclipse.collections.impl.list.mutable.FastList;
import org.tora.repositories.InMemoryRepository;

import java.util.List;

public class EclipseFastListBasedRepository<T> implements InMemoryRepository<T> {
    private final List<T> list;

    public EclipseFastListBasedRepository() {
        list = new FastList<>();
    }

    @Override
    public void add(T element) {
        list.add(element);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }

    @Override
    public int size() {
        return list.size();
    }
}