package org.tora.repositories.implementations.eclipse;

import org.eclipse.collections.impl.factory.Sets;
import org.tora.repositories.InMemoryRepository;

import java.util.Set;

public class EclipseMutableSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> set;

    public EclipseMutableSetBasedRepository() {
        set = Sets.mutable.empty();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }

    @Override
    public int size() {
        return set.size();
    }
}
