package org.tora.repositories.implementations.fastutil;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import org.tora.repositories.InMemoryRepository;

import java.util.Set;

public class FastutilSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> set;

    public FastutilSetBasedRepository() {
        set = new ObjectOpenHashSet<>();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }

    @Override
    public int size() {
        return set.size();
    }
}