package org.tora.repositories.implementations.koloboke;

import com.koloboke.collect.set.hash.HashObjSets;
import org.tora.repositories.InMemoryRepository;

import java.util.Set;

public class KolobokeSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> set;

    public KolobokeSetBasedRepository() {
        set = HashObjSets.newMutableSet();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }

    @Override
    public int size() {
        return set.size();
    }
}