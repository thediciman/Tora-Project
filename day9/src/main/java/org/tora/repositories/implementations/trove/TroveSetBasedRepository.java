package org.tora.repositories.implementations.trove;

import gnu.trove.set.hash.THashSet;
import org.tora.repositories.InMemoryRepository;

import java.util.Set;

public class TroveSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> set;

    public TroveSetBasedRepository() {
        set = new THashSet<>();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }

    @Override
    public int size() {
        return set.size();
    }
}
