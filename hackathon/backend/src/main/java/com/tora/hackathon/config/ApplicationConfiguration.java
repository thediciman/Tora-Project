package com.tora.hackathon.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

@Configuration
public class ApplicationConfiguration {
    @Bean
    public KeyHolder getKeyHolder() {
        return new GeneratedKeyHolder();
    }
}