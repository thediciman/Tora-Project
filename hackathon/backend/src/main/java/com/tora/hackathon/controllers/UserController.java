package com.tora.hackathon.controllers;

import com.tora.hackathon.entities.LoginCredentials;
import com.tora.hackathon.entities.User;
import com.tora.hackathon.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/add-user")
    public ResponseEntity<String> add(@RequestBody User user) {
        log.info("POST request for adding user: {}", user);
        try {
            userService.add(user);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/delete-user")
    public ResponseEntity<String> delete(@RequestBody User user) {
        log.info("POST request to delete user: {}", user);
        try {
            userService.deleteUser(user);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/get-user")
    public ResponseEntity<User> getUserByID(@RequestParam Long id) {
        log.info("GET request to get user by ID: {}", id);
        return new ResponseEntity<>(userService.getByID(id), HttpStatus.OK);
    }

    @GetMapping("/get-all-users")
    public ResponseEntity<List<User>> getAllUsers() {
        log.info("GET request to get all users");
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<User> requestLogin(@RequestBody LoginCredentials loginCredentials) {
        log.info("POST request to login with credentials: {}", loginCredentials);
        User user = userService.getUserFromLoginCredentials(loginCredentials);
        return new ResponseEntity<>(user, user == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
}