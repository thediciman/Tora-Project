package com.tora.hackathon.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Building {
    private int ID;
    private String name;
    private String location;
}

/*

CREATE TABLE buildings (
	id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
	name VARCHAR,
	location VARCHAR,
	PRIMARY KEY(id)
)

*/