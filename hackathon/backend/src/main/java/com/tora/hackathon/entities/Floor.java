package com.tora.hackathon.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Floor {
    private int ID;
    private int buildingID;
    private int number;
}

/*

CREATE TABLE floors (
	id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
	building_id INT,
	number INT,
	PRIMARY KEY(id)
)

*/