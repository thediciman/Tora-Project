package com.tora.hackathon.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Office {
    private int ID;
    private int floorID;
    private int number;
}

/*

CREATE TABLE offices (
	id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
	floor_id INT,
	number INT,
	PRIMARY KEY(id)
)

*/