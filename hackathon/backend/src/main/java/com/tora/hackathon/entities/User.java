package com.tora.hackathon.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String firstName;
    private String lastName;
    private LoginCredentials loginCredentials;
    private boolean isAdmin;
}

/*

CREATE TABLE users (
	id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
	first_name VARCHAR,
	last_name VARCHAR,
	username VARCHAR,
	password VARCHAR,
	is_admin BOOLEAN,
	PRIMARY KEY(id)
)

*/