package com.tora.hackathon.mappers;

import com.tora.hackathon.entities.LoginCredentials;
import com.tora.hackathon.entities.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        return new User(
                resultSet.getString("first_name"),
                resultSet.getString("last_name"),
                new LoginCredentials(
                        resultSet.getString("username"),
                        resultSet.getString("password")
                ),
                resultSet.getBoolean("is_admin")
        );
    }
}