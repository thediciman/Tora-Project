package com.tora.hackathon.repositories;

public class UserQueries {
    public static final String INSERT = "INSERT INTO users " +
            "(first_name, last_name, username, password, is_admin) VALUES " +
            "(:first_name, :last_name, :username, :password, :is_admin)";

    public static final String GET_BY_ID = "SELECT * FROM users WHERE id = ?";

    public static final String GET_BY_USERNAME = "SELECT * FROM users WHERE username = ?";

    public static final String GET_ALL = "SELECT * FROM users";

    public static final String DELETE_USER = "DELETE FROM users WHERE username = :username";
}