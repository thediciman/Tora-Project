package com.tora.hackathon.repositories;

import com.tora.hackathon.entities.User;
import com.tora.hackathon.mappers.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {
    private final KeyHolder keyHolder;
    private final JdbcTemplate jdbcTemplate;
    private final UserRowMapper userRowMapper;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public UserRepository(KeyHolder keyHolder, JdbcTemplate jdbcTemplate, UserRowMapper userRowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.keyHolder = keyHolder;
        this.jdbcTemplate = jdbcTemplate;
        this.userRowMapper = userRowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public void add(User user) {
        if (getByUsername(user.getLoginCredentials().getUsername()) != null) {
            throw new IllegalArgumentException("Username already exists.");
        }

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("first_name", user.getFirstName())
                .addValue("last_name", user.getLastName())
                .addValue("username", user.getLoginCredentials().getUsername())
                .addValue("password", user.getLoginCredentials().getPassword())
                .addValue("is_admin", user.isAdmin());

        namedParameterJdbcTemplate.update(UserQueries.INSERT, parameters, keyHolder);
    }

    public User getByID(Long id) {
        return jdbcTemplate.queryForObject(UserQueries.GET_BY_ID, new Object[]{id}, userRowMapper);
    }

    public User getByUsername(String username) {
        try {
            return jdbcTemplate.queryForObject(UserQueries.GET_BY_USERNAME, new Object[]{username}, userRowMapper);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    public void deleteUser(String username) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("username", username);

        int affectedRows = namedParameterJdbcTemplate.update(UserQueries.DELETE_USER, parameters, keyHolder);
        if (affectedRows == 0) {
            throw new IllegalArgumentException("Username not found.");
        }
    }

    public List<User> getAll() {
        return jdbcTemplate.query(UserQueries.GET_ALL, userRowMapper);
    }
}