package com.tora.hackathon.services;

import com.tora.hackathon.entities.LoginCredentials;
import com.tora.hackathon.entities.User;
import com.tora.hackathon.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void add(User user) {
        userRepository.add(user);
    }

    public void deleteUser(User user) {
        userRepository.deleteUser(user.getLoginCredentials().getUsername());
    }

    public User getByID(Long id) {
        return userRepository.getByID(id);
    }

    public List<User> getAll() {
        return userRepository.getAll();
    }

    public User getUserFromLoginCredentials(LoginCredentials loginCredentials) {
        User user = userRepository.getByUsername(loginCredentials.getUsername());
        return user == null || !user.getLoginCredentials().getPassword().equals(loginCredentials.getPassword()) ? null : user;
    }
}
