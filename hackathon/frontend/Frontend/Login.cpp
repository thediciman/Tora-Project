#include "qurl.h"
#include "Login.h"
#include "qdebug.h"
#include "qurlquery.h"
#include "qjsonobject.h"
#include "qmessagebox.h"
#include "MainDashboard.h"
#include "qjsondocument.h"
#include "QtNetwork/qnetworkrequest.h"
#include "QtNetwork/qnetworkaccessmanager.h"

Login::Login(QWidget* parent) : QWidget(parent), loginManager(new QNetworkAccessManager(this)) {
	ui.setupUi(this);
	connect(loginManager, &QNetworkAccessManager::finished, this, &Login::loginReplyFinished);
}

void Login::loginReplyFinished(QNetworkReply* reply) {
	QJsonDocument responseJsonDocument = QJsonDocument::fromJson(reply->readAll());

	if (responseJsonDocument.isEmpty()) {
		QMessageBox::critical(this, "Invalid Login", "Please check your username and password.");
	} else {
		QMessageBox::information(this, "Successful Login", QString::fromStdString("Hello, " + responseJsonDocument["firstName"].toString().toStdString() + " " + responseJsonDocument["lastName"].toString().toStdString() + "!"));

		std::string firstName = responseJsonDocument["firstName"].toString().toStdString();
		std::string lastName = responseJsonDocument["lastName"].toString().toStdString();
		std::string username = responseJsonDocument["loginCredentials"]["username"].toString().toStdString();
		bool isAdmin = responseJsonDocument["admin"].toBool();

		MainDashboard* mainDashboard = new MainDashboard(firstName, lastName, username, isAdmin);
		mainDashboard->show();
		this->close();
	}
}

Login::~Login() {}

void Login::handleSignIn() {
	QUrl url("http://localhost:8080/login");

	QJsonObject jsonRequestBody;
	jsonRequestBody["username"] = ui.usernameLineEdit->text();
	jsonRequestBody["password"] = ui.passwordLineEdit->text();

	QNetworkRequest signInRequest(url);
	signInRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

	loginManager->post(signInRequest, QJsonDocument(jsonRequestBody).toJson());
}