#pragma once

#include <QWidget>
#include "ui_Login.h"
#include "QtNetwork/qnetworkreply.h"

class Login : public QWidget {
	Q_OBJECT

public:
	Login(QWidget* parent = Q_NULLPTR);
	~Login();

private:
	QNetworkAccessManager* loginManager;
	Ui::Login ui;

private slots:
	void loginReplyFinished(QNetworkReply* reply);
	void handleSignIn();
};
