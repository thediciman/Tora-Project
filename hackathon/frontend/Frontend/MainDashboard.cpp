#include "qdebug.h"
#include "qjsonarray.h"
#include "qjsonobject.h"
#include "qmessagebox.h"
#include "MainDashboard.h"
#include "qjsondocument.h"
#include "QtNetwork/qnetworkreply.h"
#include "QtNetwork/qnetworkaccessmanager.h"

MainDashboard::MainDashboard(const std::string& firstName, const std::string& lastName, const std::string& username, bool isAdmin, QWidget* parent) :
	QWidget(parent),
	getAllUsersManager(new QNetworkAccessManager(this)),
	addUserManager(new QNetworkAccessManager(this)),
	deleteUserManager(new QNetworkAccessManager(this)) {

	ui.setupUi(this);

	ui.arrivalDateTimeEdit->setDisplayFormat("dd/MM/yyyy hh:mm:ss");
	ui.departureDateTimeEdit->setDisplayFormat("dd/MM/yyyy hh:mm:ss");

	ui.arrivalDateTimeEdit->setDate(QDate::currentDate());
	ui.departureDateTimeEdit->setDate(QDate::currentDate());

	ui.buildingComboBox->addItem("default");
	ui.floorComboBox->addItem("default");
	ui.officeComboBox->addItem("default");

	if (!isAdmin) {
		ui.tabWidget->removeTab(2);
		ui.tabWidget->removeTab(1);
	} else {
		connect(getAllUsersManager, &QNetworkAccessManager::finished, this, &MainDashboard::getAllUsersReplyFinished);
		connect(addUserManager, &QNetworkAccessManager::finished, this, &MainDashboard::addUserReplyFinished);
		connect(deleteUserManager, &QNetworkAccessManager::finished, this, &MainDashboard::deleteUserReplyFinished);

		requestUsersAndPopulate();
	}
}

void MainDashboard::requestUsersAndPopulate() {
	QUrl url("http://localhost:8080/get-all-users");
	QNetworkRequest getAllUsersRequest(url);
	getAllUsersManager->get(getAllUsersRequest);
}

void MainDashboard::addUserReplyFinished(QNetworkReply* reply) {
	QByteArray responseByteArray = reply->readAll();
	if (responseByteArray.size() == 0) {
		QMessageBox::information(this, "Successful Operation", "The user has been added successfully!");

		requestUsersAndPopulate();

		ui.firstNameLineEdit->clear();
		ui.lastNameLineEdit->clear();
		ui.usernameLineEdit->clear();
		ui.passwordLineEdit->clear();
		ui.adminCheckBox->setChecked(false);
	} else {
		QMessageBox::critical(this, "Unsuccessful Operation", responseByteArray);
	}
}

void MainDashboard::deleteUserReplyFinished(QNetworkReply* reply) {
	QByteArray responseByteArray = reply->readAll();
	if (responseByteArray.size() == 0) {
		QMessageBox::information(this, "Successful Operation", "The user has been removed successfully!");
		requestUsersAndPopulate();
	} else {
		QMessageBox::critical(this, "Unsuccessful Operation", responseByteArray);
	}
}

void MainDashboard::getAllUsersReplyFinished(QNetworkReply* reply) {
	QJsonArray responseJsonArray = QJsonDocument::fromJson(reply->readAll()).array();

	users.clear();
	for (int i = 0; i < responseJsonArray.size(); ++i) {
		users.push_back(responseJsonArray[i]);
	}

	populateUsersList();
}

void MainDashboard::populateUsersList() {
	ui.listWidget->clear();
	for (const QJsonValue& currentObject : users) {
		new QListWidgetItem(QString::fromStdString(currentObject["firstName"].toString().toStdString() + " " + currentObject["lastName"].toString().toStdString() + ", " + (currentObject["admin"].toBool() ? "Admin" : "Normal User")), ui.listWidget);
	}
}

void MainDashboard::handleAddUser() {
	QJsonObject jsonRequestBody;

	QJsonObject loginCredentials;
	loginCredentials["username"] = ui.usernameLineEdit->text();
	loginCredentials["password"] = ui.passwordLineEdit->text();

	jsonRequestBody["firstName"] = ui.firstNameLineEdit->text();
	jsonRequestBody["lastName"] = ui.lastNameLineEdit->text();
	jsonRequestBody["admin"] = ui.adminCheckBox->isChecked();
	jsonRequestBody["loginCredentials"] = loginCredentials;

	QUrl url("http://localhost:8080/add-user");

	QNetworkRequest signInRequest(url);
	signInRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

	addUserManager->post(signInRequest, QJsonDocument(jsonRequestBody).toJson());
}

void MainDashboard::handleRemoveUser() {
	int index = getSelectedIndex(ui.listWidget);

	if (index == -1) {
		QMessageBox::warning(this, "Warning", "Please select a user.");
		return;
	}

	QJsonValue selectedUser = users[index];

	QUrl url("http://localhost:8080/delete-user");

	QNetworkRequest signInRequest(url);
	signInRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

	deleteUserManager->post(signInRequest, QJsonDocument(selectedUser.toObject()).toJson());
}

int MainDashboard::getSelectedIndex(QListWidget* listWidget) {
	QModelIndexList selectedIndices = ui.listWidget->selectionModel()->selectedIndexes();

	if (selectedIndices.size() == 0) {
		return -1;
	}

	return selectedIndices[0].row();
}

MainDashboard::~MainDashboard() {}