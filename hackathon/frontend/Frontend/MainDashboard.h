#pragma once

#include <vector>
#include <QWidget>
#include "ui_MainDashboard.h"
#include "QtNetwork/qnetworkaccessmanager.h"

class MainDashboard : public QWidget {
	Q_OBJECT

public:
	MainDashboard(const std::string& firstName, const std::string& lastName, const std::string& username, bool isAdmin, QWidget* parent = Q_NULLPTR);
	~MainDashboard();

private:
	std::vector<QJsonValue> users;
	QNetworkAccessManager* getAllUsersManager;
	QNetworkAccessManager* addUserManager;
	QNetworkAccessManager* deleteUserManager;
	Ui::MainDashboard ui;

	void populateUsersList();
	int getSelectedIndex(QListWidget* listWidget);
	void requestUsersAndPopulate();

private slots:
	void getAllUsersReplyFinished(QNetworkReply* reply);
	void addUserReplyFinished(QNetworkReply* reply);
	void deleteUserReplyFinished(QNetworkReply* reply);

	void handleAddUser();
	void handleRemoveUser();
};