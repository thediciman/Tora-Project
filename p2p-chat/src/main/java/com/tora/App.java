package com.tora;

import java.util.Scanner;

public class App {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Your name: ");
        String username = scanner.nextLine();

        Peer peer = new Peer(username);
        peer.receiveMessage();

        while (true) {
            String input = scanner.nextLine();
            String[] inputTokens = input.split("\\s+", 3);

            if (input.equals("!byebye")) {
                break;
            }

            String consumer = inputTokens[1];

            if (inputTokens[0].equals("!hello")) {
                peer.sendMessage(new Message("!hello", username, consumer));
            }

            if (input.contains("!sendto")) {
                String message = inputTokens[2];
                peer.sendMessage(new Message(message, username, consumer));
            }
        }

        peer.closeConnection();
    }

}