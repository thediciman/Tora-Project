package com.tora;

import org.json.JSONObject;

import java.util.Objects;

public class Message {
    private final String message;
    private final String from;
    private final String to;

    public Message(String message, String from, String to) {
        this.message = message;
        this.from = from;
        this.to = to;
    }

    public static Message fromJSON(String json) {
        JSONObject object = new JSONObject(json);

        return new Message(object.getString("message"),
                object.getString("from"),
                object.getString("to"));
    }

    public String getMessage() {
        return message;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String createJSON() {
        JSONObject object = new JSONObject();
        object.put("message", getMessage());
        object.put("from", getFrom());
        object.put("to", getTo());

        return object.toString();
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(message, message1.message) &&
                Objects.equals(from, message1.from) &&
                Objects.equals(to, message1.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, from, to);
    }
}