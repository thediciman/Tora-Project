package com.tora;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class Peer {

    private static final int N_THREADS = 10;

    private final Executor sendExecutor = Executors.newFixedThreadPool(N_THREADS);
    private final Executor recvExecutor = Executors.newSingleThreadExecutor();

    private final String username;

    private final Connection connection;
    private final Channel channel;

    private final Map<String, Boolean> connectedPeers = new ConcurrentHashMap<>();

    public Peer(String username) throws IOException, TimeoutException, NoSuchAlgorithmException, KeyManagementException, URISyntaxException {
        this.username = username;

        ConnectionFactory factory = new ConnectionFactory();
        String uri = "amqp://iijxrbbq:rnaStNIloFgQAQXtvYBk7A612BQelBr8@chinook.rmq.cloudamqp.com/iijxrbbq";
        factory.setUri(uri);
        factory.setRequestedHeartbeat(30);
        factory.setConnectionTimeout(30000);

        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare(username, false, false, false, null);
    }

    public String getUsername() {
        return username;
    }

    public Channel getChannel() {
        return channel;
    }

    public Map<String, Boolean> getConnectedPeers() {
        return connectedPeers;
    }

    public void addConnectedPeer(String username) {
        connectedPeers.put(username, Boolean.TRUE);
    }

    public void sendMessage(Message message) {
        sendExecutor.execute(new SendTask(message, channel));
    }

    public void receiveMessage() {
        recvExecutor.execute(new ReceiveTask(this));
    }

    public void closeConnection() throws IOException, TimeoutException {
        channel.close();
        connection.close();
    }

}