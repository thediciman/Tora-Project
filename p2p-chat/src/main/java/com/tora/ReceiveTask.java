package com.tora;

import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ReceiveTask implements Runnable {

    private final Peer peer;

    public ReceiveTask(Peer peer) {
        this.peer = peer;
    }

    @Override
    public void run() {
        try {
            peer.getChannel().basicConsume(peer.getUsername(), true, getDeliverCallback(),
                    consumerTag -> {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized DeliverCallback getDeliverCallback() {
        return (consumerTag, delivery) -> {
            String json = new String(delivery.getBody(), StandardCharsets.UTF_8);
            Message message = Message.fromJSON(json);

            System.out.println(message);

            if (message.getMessage().equals("!hello")) {
                System.out.println(message.getFrom() + " wants to connect with you!");
                peer.sendMessage(new Message("!ack", message.getTo(), message.getFrom()));
                peer.addConnectedPeer(message.getFrom());
            } else {
                if (message.getMessage().equals("!ack")) {
                    peer.addConnectedPeer(message.getFrom());
                } else {
                    if (isPeerPresent(message.getFrom())) {
                        System.out.println("[" + message.getFrom() + "]: " + message.getMessage());
                    }
                }
            }
        };
    }

    private synchronized boolean isPeerPresent(String username) {
        return peer.getConnectedPeers().getOrDefault(username, false);
    }

}