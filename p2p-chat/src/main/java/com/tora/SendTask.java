package com.tora;

import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class SendTask implements Runnable {

    private final Message message;
    private final Channel channel;

    public SendTask(Message message, Channel channel) {
        this.message = message;
        this.channel = channel;
    }

    @Override
    public void run() {
        try {
            channel.basicPublish("", message.getTo(), null,
                    message.createJSON().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
