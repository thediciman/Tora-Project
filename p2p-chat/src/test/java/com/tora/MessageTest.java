package com.tora;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MessageTest {

    @Test
    public void createJSON() {
        Message message = new Message("contents", "user1", "user2");
        assertEquals("{\"from\":\"user1\",\"to\":\"user2\",\"message\":\"contents\"}", message.createJSON());
    }

    @Test
    public void fromJSON() {
        String contentJson = "{\"from\":\"user1\",\"to\":\"user2\",\"message\":\"contents\"}";
        Message expected = new Message("contents", "user1", "user2");
        assertEquals(expected, Message.fromJSON(contentJson));
    }

}