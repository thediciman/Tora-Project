package com.tora;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class PeerTest {

    @Test
    public void testArbitrarySend() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);

        PrintStream old = System.out;

        System.setOut(ps);
        System.out.flush();

        Peer peer = new Peer("testPeer1");
        peer.receiveMessage();

        peer.sendMessage(new Message("content", "testPeer1", "testPeer1"));

        TimeUnit.MILLISECONDS.sleep(1000);

        System.setOut(old);

        assertEquals("Message{message='content', from='testPeer1', to='testPeer1'}\r\n", baos.toString());
    }

    @Test
    public void testInitiateConnection() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);

        PrintStream old = System.out;

        System.setOut(ps);
        System.out.flush();

        Peer peer = new Peer("testPeer2");
        peer.receiveMessage();

        peer.sendMessage(new Message("!hello", "testPeer2", "testPeer2"));

        TimeUnit.MILLISECONDS.sleep(1000);

        System.setOut(old);

        assertEquals("Message{message='!hello', from='testPeer2', to='testPeer2'}\r\n" +
                "testPeer2 wants to connect with you!\r\n" +
                "Message{message='!ack', from='testPeer2', to='testPeer2'}\r\n", baos.toString());
    }

}